package models;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
@Entity
public class Product extends Model{
	@Constraints.Required
  public String ean;
	@Constraints.Required
  public String name;
  public String description;
  @OneToMany(mappedBy="product")
  public List<StockItem> stockItems;
  @ManyToMany
  public List<Tag> tags = new LinkedList<Tag>();
  public static Finder<Long,Product> find = new Finder<Long,Product>(
            Long.class, Product.class);
  public Product() {}
  public Product(String ean, String name, String description) {
    this.ean = ean;
    this.name = name;
    this.description = description;
  }
  public String toString() {
    return String.format("%s - %s", ean, name);
  }
  private static List<Product> products;
  static {
    products = new ArrayList<Product>();
    products.add(new Product("1111111111111", "Paperclips 1",
        "Paperclips description 1"));
    products.add(new Product("2222222222222", "Paperclips 2",
        "Paperclips description 2"));
    products.add(new Product("3333333333333", "Paperclips 3",
        "Paperclips description 3"));
    products.add(new Product("4444444444444", "Paperclips 4",
        "Paperclips description 4"));
    products.add(new Product("5555555555555", "Paperclips 5",
        "Paperclips description 5"));
  }
  
  
	public static List<Product> findAll() {
    return new ArrayList<Product>(products);
	}
	public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }
  
	public static List<Product> findByName(String term) {
        return find.where().eq("name", term).findList();
    }
 
  public static boolean remove(Product product) {
    return products.remove(product);
  }
 
  public void save() {
    products.remove(findByEan(this.ean));
    products.add(this);
  }
  

}