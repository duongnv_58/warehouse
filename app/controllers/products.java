package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.list;
import play.data.Form;
import views.html.products.details;
import java.util.ArrayList;
import java.util.List;
import models.Product;
import models.Tag;
public class products extends Controller {
	private static final Form<Product> productForm = Form.form(Product.class);
	public static Result list(){
		List<Product> products = Product.findAll();
		return ok(list.render(products));
	}
	public static Result newProduct(){
		return ok(details.render(productForm));
	}
	public static Result details(String ean){
		final Product product = Product.findByEan(ean);
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	public static Result save(){
		Form<Product> boundForm = productForm.bindFromRequest();
		Product product = boundForm.get();
		Ebean.save(product);
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		if (product.id == null) {
            product.save();
        } else {
            product.update();
        }

        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
        stockItem.save();

        flash("success", String.format("Successfully added product %s", product));
        return redirect(routes.Products.list(0));

	}
	public static Result delete(String ean) {
		Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
        product.delete();
        return redirect(routes.Products.list(0));
    }
}